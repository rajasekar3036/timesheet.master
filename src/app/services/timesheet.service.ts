import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material';


export interface Timesheet {
    week: Week;
    taskList: TaskListItems;
    summary: Summary;
}

export interface Week {
    prev: Date;
    current_week_start: Date;
    current_week_end: Date;
    current: Date;
    next: Date;
}

export interface Task {
    taskDate: Date;
    hoursWorked: number;
    day: string;
    taskId: number;
    employeeId: number;
    id: number;
}

export interface TaskItems extends Array<Task> { }

export interface TaskList {
    taskId: number;
    taskName: string;
    sunday: Task;
    monday: Task;
    tuesday: Task;
    wednesday: Task;
    thursday: Task;
    friday: Task;
    saturday: Task;
}
export interface TaskListItems extends Array<TaskList> { }

export interface Summary {
    sunday: Task;
    monday: Task;
    tuesday: Task;
    wednesday: Task;
    thursday: Task;
    friday: Task;
    saturday: Task;
}


@Injectable()
export class TimesheetService {
    private baseapi = environment.apiUrl;
    empId: Number;

    constructor(private http: HttpClient) { }


    gettimesheet(id: number) {
        return this.http.get(this.baseapi + `/timesheet/get-employee/?id=${id}`);
    }

    gettimesheetCurrentweek(id: number) {
        this.empId = id;
        var today = new Date();
        //var currDate = this.toShortFormat(today);
        return this.gettimesheetweek(id, today);
        //return this.http.get<Timesheet>(this.baseapi + `/timesheet/get-week/?id=${id}&currentDate=1-Jan-2019`);
    }


    gettimesheetweek(id: number, week: Date) {
        this.empId = id;
        var currDate = this.toShortFormat(week);
        var url = this.baseapi + `/timesheet/get-week/?id=${id}&currentDate=${currDate}`;
        console.log(url);
        return this.http.get<Timesheet>(url);
    }

    savetimesheet(data, callNext) {
        return this.http.post(this.baseapi + `/timesheet/save/`, data).subscribe(data => {
            //console.log(data);
            if (callNext) {
                callNext(data);
            }
        });
    }

    toShortFormat(currDate:Date) {
        var month_names = ["Jan", "Feb", "Mar",
            "Apr", "May", "Jun",
            "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"];
        var day = currDate.getDate();
        var month_index = currDate.getMonth();
        var year = currDate.getFullYear();

        return "" + day + "-" + month_names[month_index] + "-" + year;
    }

    addDays(currDate:Date, days:number) {
        var date = new Date(currDate);
        date.setDate(date.getDate() + days);
        //var dataStr= this.toShortFormat(date)
        return date;
    }


    addRow(dataSource: MatTableDataSource<TaskList>, employeeId, week_start) {
        var timesheet: Array<TaskList> = dataSource.data;
        var tl: TaskList =
        {
            taskId: -1,
            taskName: "",
            sunday: {
                id: -1,
                taskDate: week_start,
                hoursWorked: 0,
                day: "Sunday",
                taskId: -1,
                employeeId: employeeId
            },
            monday: {
                id: -1,
                taskDate: this.addDays(week_start, 1),
                hoursWorked: 0,
                day: "Monday",
                taskId: -1,
                employeeId: employeeId
            },
            tuesday: {
                id: -1,
                taskDate: this.addDays(week_start, 2),
                hoursWorked: 0,
                day: "Tuesday",
                taskId: -1,
                employeeId: employeeId
            },
            wednesday: {
                id: -1,
                taskDate: this.addDays(week_start, 3),
                hoursWorked: 0,
                day: "Wednesday",
                taskId: -1,
                employeeId: employeeId
            },
            thursday: {
                id: -1,
                taskDate: this.addDays(week_start, 4),
                hoursWorked: 0,
                day: "Thursday",
                taskId: -1,
                employeeId: employeeId
            },
            friday: {
                id: -1,
                taskDate: this.addDays(week_start, 5),
                hoursWorked: 0,
                day: "Friday",
                taskId: -1,
                employeeId: employeeId
            },
            saturday: {
                id: -1,
                taskDate: this.addDays(week_start, 6),
                hoursWorked: 0,
                day: "Saturday",
                taskId: -1,
                employeeId: employeeId
            }
        };
        timesheet.push(tl);
    }
}