import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { TimesheetListComponent }   from './timesheet/timesheet.component';
import { EmployeeListComponent }      from './employee/employee.component';
 
const routes: Routes = [
  { path: '', redirectTo: '/employee', pathMatch: 'full' },
  { path: 'employee', component: EmployeeListComponent },
  { path: 'timesheet', component: TimesheetListComponent },
  { path: 'timesheet/:id', component: TimesheetListComponent },
  { path: 'timesheet/:id/:week', component: TimesheetListComponent }
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}