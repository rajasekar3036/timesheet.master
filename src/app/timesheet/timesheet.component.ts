import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TimesheetService ,Timesheet,TaskList,TaskListItems,Task} from '../services/timesheet.service';
import { EmployeeService } from '../services/employee.service';
import { TaskService } from '../services/task.service';
import { RouterModule, Routes,Router } from '@angular/router';
import {MatSnackBar,MatTableDataSource} from '@angular/material';
//import { TLSSocket } from 'tls';
//import { Type } from '@angular/compiler';

@Component({
    selector: 'timesheet-list',
    templateUrl: 'timesheet.component.html',
    styleUrls:['./timesheet.component.scss']
})

export class TimesheetListComponent implements OnInit {
   
    @Input() timesheet: Timesheet;
    dataSource: MatTableDataSource<TaskList>;
    employees: any;
    tasks: any;
    displayedColumns: string[] = ['name','sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
    employeeId: number;
    constructor(private timesheetService: TimesheetService, private taskService: TaskService, private employeeService: EmployeeService, private route: ActivatedRoute, private router: Router,private snackBar: MatSnackBar) { }

    ngOnInit() { 
        
        this.employeeService.getallemployees().subscribe(data => {
            this.employees = data;
        });

        this.taskService.getalltasks().subscribe(data => {
            this.tasks = data;
        });
         
        //inplace route change 
        this.route.params.subscribe(params => {
            const id = +this.route.snapshot.paramMap.get('id');
            const week = this.route.snapshot.paramMap.get('week');
            
                if(week==null || week.length==0){
                    this.employeeId = id;
                    this.timesheetService.gettimesheetCurrentweek(id).subscribe(data => {                        
                        this.timesheet = data;     
                        this.dataSource = new MatTableDataSource(this.timesheet.taskList);       
                        for (const key in  this.timesheet.taskList) {                          
                            if ( this.timesheet.taskList.hasOwnProperty(key)) {
                                const element =  this.timesheet.taskList[key];
                                element.sunday.employeeId=id;
                                element.monday.employeeId=id;
                                element.tuesday.employeeId=id;
                                element.wednesday.employeeId=id;
                                element.tuesday.employeeId=id;
                                element.friday.employeeId=id;
                                element.saturday.employeeId=id;
                            }                                                                  
                        }  
                    });
                }else{
                    console.log(week);
                    this.employeeId = parseInt(id.toString());
                    var dateVal = new Date(week);
                    this.timesheetService.gettimesheetweek(id,dateVal).subscribe(data => {                        
                        this.timesheet = data;     
                        this.dataSource = new MatTableDataSource(this.timesheet.taskList);       
                        console.log(this.timesheet.week);
                        for (const key in  this.timesheet.taskList) {                          
                            if ( this.timesheet.taskList.hasOwnProperty(key)) {
                                const element =  this.timesheet.taskList[key];
                                element.sunday.employeeId=id;
                                element.monday.employeeId=id;
                                element.tuesday.employeeId=id;
                                element.wednesday.employeeId=id;
                                element.tuesday.employeeId=id;
                                element.friday.employeeId=id;
                                element.saturday.employeeId=id;
                            }                                                                  
                        }  
                    });
                } 
        });

        
    }

    add(){
        var week_start = this.timesheet.week.current_week_start;
        this.timesheetService.addRow(this.dataSource,this.employeeId,week_start); 
        this.dataSource.data = [...this.dataSource.data];
        console.log(this.timesheet.taskList);     
    }

    save(){
       var data=[];
       for (const key in  this.timesheet.taskList) {        
           if ( this.timesheet.taskList.hasOwnProperty(key)) {
               const element =  this.timesheet.taskList[key];    
               element.sunday.taskId = element.taskId;
               element.monday.taskId = element.taskId;
               element.tuesday.taskId = element.taskId;
               element.wednesday.taskId = element.taskId;
               element.thursday.taskId = element.taskId;
               element.friday.taskId = element.taskId;
               element.saturday.taskId = element.taskId;

               element.sunday.employeeId =  this.employeeId;
               element.monday.employeeId =  this.employeeId;
               element.tuesday.employeeId =  this.employeeId;
               element.wednesday.employeeId =  this.employeeId;
               element.thursday.employeeId =  this.employeeId;
               element.friday.employeeId =  this.employeeId;
               element.saturday.employeeId =  this.employeeId; 

               if(element.sunday.id>0 || parseInt(element.sunday.hoursWorked.toString())>0) data.push(element.sunday);
               if(element.monday.id>0 || parseInt(element.monday.hoursWorked.toString())>0) data.push(element.monday);
               if(element.tuesday.id>0 || parseInt(element.tuesday.hoursWorked.toString())>0) data.push(element.tuesday);
               if(element.wednesday.id>0 || parseInt(element.wednesday.hoursWorked.toString())>0) data.push(element.wednesday);
               if(element.thursday.id>0 || parseInt(element.thursday.hoursWorked.toString())>0) data.push(element.thursday);
               if(element.friday.id>0 || parseInt(element.friday.hoursWorked.toString())>0) data.push(element.friday);
               if(element.saturday.id>0 || parseInt(element.sunday.hoursWorked.toString())>0) data.push(element.saturday);
           }           
       }   
       var  snackBar = this.snackBar;
       var employeeId = this.employeeId;
       var router = this.router;
       var current_data = this.timesheet.week.current;
        console.log( data);
        this.timesheetService.savetimesheet(data,function(returnData){
            if(returnData>=0){ 
                router.navigate(['/timesheet/', employeeId , current_data]);
                //router.navigate(['/timesheet/', employeeId]);               
                snackBar.openFromComponent(SnackBarComponent, {
                    duration: 1500,
                  });
            }else{
                snackBar.open("Error While saving", "Close", {
                    duration: 1500,
                  });
            }
            
        });
    } 

    GetTaskSelectStatus(taskId){
        if(taskId<=0){
            return "disabled";
        }else 
            return "";
    }

    employeeChanged(value){
        this.router.navigate(['/timesheet/', value]);
    }

    prevWeek(){        
        this.router.navigate(['/timesheet/', this.employeeId , this.timesheet.week.prev]);
    }

    nextWeek(){        
        this.router.navigate(['/timesheet/', this.employeeId , this.timesheet.week.next]);
    }

    /** Gets the total cost of all transactions. */
    getSundayCost() { 
        if(this.timesheet!=undefined)
        return  this.timesheet.taskList.map(t => t.sunday.hoursWorked).reduce((acc, value) => {  var val = parseInt(value.toString()) ; return acc +val ; }, 0);
        else return 0;
    }
    getMondayCost() {
        if(this.timesheet!=undefined)
        return  this.timesheet.taskList.map(t => t.monday.hoursWorked).reduce((acc, value) => {  var val = parseInt(value.toString()) ; return acc +val ; }, 0);
        else return 0;
    }
    getTuesdayCost() {
        if(this.timesheet!=undefined)
        return  this.timesheet.taskList.map(t => t.tuesday.hoursWorked).reduce((acc, value) => {  var val = parseInt(value.toString()) ; return acc +val ; }, 0);
        else return 0;
    }
    getWednesdayCost() {
        if(this.timesheet!=undefined)
        return  this.timesheet.taskList.map(t => t.wednesday.hoursWorked).reduce((acc, value) => {  var val = parseInt(value.toString()) ; return acc +val ; }, 0);
        else return 0;
    }
    getThursdayCost() {
        if(this.timesheet!=undefined)
        return  this.timesheet.taskList.map(t => t.thursday.hoursWorked).reduce((acc, value) => {  var val = parseInt(value.toString()) ; return acc +val ; }, 0);
        else return 0;
    }
    getFridayCost() {
        if(this.timesheet!=undefined)
        return  this.timesheet.taskList.map(t => t.friday.hoursWorked).reduce((acc, value) => {  var val = parseInt(value.toString()) ; return acc +val ; }, 0);
        else return 0;
    }
    getSaturdayCost() {
        if(this.timesheet!=undefined)
        return  this.timesheet.taskList.map(t => t.saturday.hoursWorked).reduce((acc, value) => {  var val = parseInt(value.toString()) ; return acc +val ; }, 0);
        else return 0;
    }
}


@Component({
    selector: 'snack-bar-saved',
    templateUrl: 'snack-bar-saved.html',
    styles: [`
      .success-dialog {
        color: hotpink;
      }
    `],
  })
  export class SnackBarComponent {}