import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }    from '@angular/forms';

import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TaskService } from './services/task.service';


import { TimesheetListComponent,SnackBarComponent } from './timesheet/timesheet.component';
import { TimesheetService } from './services/timesheet.service';

import { AppRoutingModule }     from './app-routing.module';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatNativeDateModule} from '@angular/material';
import {DemoMaterialModule} from '../material-module'; 

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetListComponent,
    SnackBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    DemoMaterialModule
  ],
  providers: [
    EmployeeService,
    TimesheetService,
    TaskService
  ], 
  entryComponents: [AppComponent,SnackBarComponent], 
  bootstrap: [AppComponent] 
})
export class AppModule  implements OnInit{
  ngOnInit() {
    console.log("AppModule");
  }
 }
